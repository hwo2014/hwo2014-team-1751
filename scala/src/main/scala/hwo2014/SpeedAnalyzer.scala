package hwo2014

object SpeedAnalyzer {
  // First qualifying value
  var magicVar = 0.44d
  //var magicVar = 0.2d

  //val curveSpeeds = scala.collection.mutable.Map[Int, Double](0 -> 3d, 40 -> 4d, 60 -> 5.4d, 90 -> 5.7d, 110 -> 6.8d, 180 -> 8.6d, 200 -> 9.1d, 220 -> 9.6d)
  def getSafeSpeed(radius: Int): Double = {
    // v^2/r = constant
    val v = math.sqrt(magicVar * radius)
    //v
    if (radius <= 50) v - 0.2
    else v
  }

  def decreaseMagicVar() = {
    magicVar -= 0.02
    Logger.logLine(s"Decreased magicVar to $magicVar")
  }
}