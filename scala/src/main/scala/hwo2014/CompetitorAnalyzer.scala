package hwo2014

import scala.collection.mutable.Map
import scala.collection.mutable.ListBuffer

object CarData {
  def apply(cp: CarPosition, bot: Bot) = {
    val currentPiece = bot.course(cp.piecePosition.pieceIndex)
    val position = currentPiece.absolutePosition + cp.piecePosition.inPieceDistance

    new CarData(
      cp.piecePosition.pieceIndex,
      cp.piecePosition.inPieceDistance,
      cp.angle,
      position,
      //bot.currentDistance - position,
      position - bot.currentDistance,
      cp.piecePosition.lane,
      0)
  }

  def apply(oldCp: CarPosition, cp: CarPosition, bot: Bot) = {
    val currentPiece = bot.course(cp.piecePosition.pieceIndex)
    val position = currentPiece.absolutePosition + cp.piecePosition.inPieceDistance

    new CarData(
      cp.piecePosition.pieceIndex,
      cp.piecePosition.inPieceDistance,
      cp.angle,
      position,
      //bot.currentDistance - position,
      position - bot.currentDistance,
      cp.piecePosition.lane,
      bot.calculateSpeed(Some(cp), Some(oldCp)))
  }

}

case class AiCurveData(
  val color: String,
  val curveIndex: Int,
  val radius: Int,
  val lane: Int,
  var minSpeed: Double,
  var maxSpeed: Double,
  var minAngle: Double,
  var maxAngle: Double,
  var switched: Boolean,
  var crashDelay: Int = 3)

case class CarData(
  var pieceIndex: Int,
  var piecePos: Double,
  var angle: Double,
  var position: Double,
  var distance: Double,
  var lane: LanePosition,
  var speed: Double) {

  def update(cd: CarData) {
    position = cd.position
    distance = cd.distance
    lane = cd.lane
    speed = cd.speed
  }
}

class CompetitorAnalyzer(bot: Bot) {
  val cars: Map[String, CarData] = Map()
  val aiCurveDatas: Map[String, AiCurveData] = Map()
  var pendingUpdates: ListBuffer[(String, AiCurveData, CurveLane)] = ListBuffer()

  var oldCps: CarPositions = CarPositions("", Nil, "", None)

  private def aiEnteredNewCurve(color: String, current: CarData) = {
    Logger.logLine(s"$color entered curve")
    val newCurve = SpeedLimiter.curves.find(c => c.index <= current.pieceIndex && c.endIndex >= current.pieceIndex)
    if (newCurve.isDefined) {
      aiCurveDatas.put(color, AiCurveData(color,
        current.pieceIndex,
        newCurve.get.lanes.find(_.index == current.lane.endLaneIndex).get.laneRadius,
        current.lane.endLaneIndex,
        current.speed,
        current.speed,
        current.angle,
        current.angle,
        false))
    }
  }

  private def checkPendingUpdates() = {
    pendingUpdates.foreach(p => p._2.crashDelay -= 1)
    pendingUpdates.filter(p => p._2.crashDelay == 0).foreach { p =>
      val curveLane = p._3
      val oldLimit = if (curveLane.manualSpeedLimit.isDefined) {
        curveLane.manualSpeedLimit.get
      } else {
        SpeedAnalyzer.getSafeSpeed(curveLane.laneRadius)
      }
      val newLimit = p._2.minSpeed
      val learnValue = (oldLimit + newLimit)/2
      curveLane.manualSpeedLimit = Some(learnValue)
      Logger.logLine(s"LEARNED CURVE SPEED! ${learnValue}")
    }
    pendingUpdates = pendingUpdates.filterNot(p => p._2.crashDelay == 0)
  }

  // Should not learn curvespeed, if opponent crashes instantly after curve
  def crashed(color: String) = {
    if (pendingUpdates.find(_._1 == color).isDefined) {
      Logger.logLine(s"$color crashed after curve. Do not learn curve speed")
      pendingUpdates = pendingUpdates.filterNot(p => p._1 == color)
    }
  }

  private def aiLeftCurve(color: String, curve: Option[Curve], current: CarData, curveData: AiCurveData) = {
    Logger.logLine(s"$color left curve")
    if (curve.isDefined) {

      val curveLane = curve.get.lanes.find(_.index == current.lane.endLaneIndex).get

      val calculatedSpeed = SpeedAnalyzer.getSafeSpeed(curveLane.laneRadius)
      Logger.logLine(s"$color left curve. Lane ${curveData.lane}, speed: ${curveData.minSpeed} - ${curveData.maxSpeed} vs ${calculatedSpeed} Angle: ${curveData.minAngle} - ${curveData.maxAngle}")

      if (!curveData.switched && curveLane.manualSpeedLimit.getOrElse(calculatedSpeed) < curveData.minSpeed) {
        pendingUpdates.append((color, curveData, curveLane))
      }
      // Clear aiCurveData
      aiCurveDatas -= color
    }

  }

  def updateCurveData(color: String, old: CarData, current: CarData) = {
    // TODO create/update/Fill curvedata
    // If just entered limited area
    // -> add curveData
    // If still on same curve
    // -> update curveData
    // If just left curve
    // -> Update
    // TODO should wait for few turns for crash check?

    // If already have curveData
    val lastAiCurveData = aiCurveDatas.get(color)
    if (lastAiCurveData.isDefined) {

      // curve that ai was on last turn
      val curve = SpeedLimiter.curves.find(c => c.index <= lastAiCurveData.get.curveIndex && c.endIndex >= lastAiCurveData.get.curveIndex)

      // Check if still on same curve
      val stillOnSameCurve = curve.isDefined && curve.get.index <= current.pieceIndex && curve.get.endIndex >= current.pieceIndex
      if (stillOnSameCurve) {
        if (current.lane.endLaneIndex != lastAiCurveData.get.lane) {
          // SWITCH ON CURVE? TRASH CHANGES!
          lastAiCurveData.get.switched = true
        } else {
          // update curve data
          if (current.speed > lastAiCurveData.get.maxSpeed) {
            lastAiCurveData.get.maxSpeed = current.speed
          } else if (current.speed < lastAiCurveData.get.minSpeed) {
            lastAiCurveData.get.minSpeed = current.speed
          }

          if (math.abs(current.angle) > lastAiCurveData.get.maxAngle) {
            lastAiCurveData.get.maxAngle = math.abs(current.angle)
          } else if (math.abs(current.angle) < lastAiCurveData.get.minAngle) {
            lastAiCurveData.get.minAngle = math.abs(current.angle)
          }

        }

      } else {
        // Ai moved from curve
        aiLeftCurve(color, curve, current, lastAiCurveData.get)

        // if entered curve
        val newCurve = SpeedLimiter.curves.find(c => c.index <= current.pieceIndex && c.endIndex >= current.pieceIndex)
        if (newCurve.isDefined) {
          aiEnteredNewCurve(color, current)
        }
      }
    }

    if (lastAiCurveData.isEmpty) {
      // If on curve create new data
      val newCurve = SpeedLimiter.curves.find(c => c.index <= current.pieceIndex && c.endIndex >= current.pieceIndex)
      if (newCurve.isDefined) {
        aiEnteredNewCurve(color, current)
      }
    }
  }

  def update(cps: CarPositions) = {
    if (cars.isEmpty) {
      cps.data.foreach(cp => {
        val lastCarData = cars.get(cp.id.color)
        val currentCarData = CarData(cp, bot)
        // Track opponents
        if (cp.id.color != bot.myCar.color) {
          cars.put(cp.id.color, currentCarData)
        }
        // Learn from all cars
        if (lastCarData.isDefined) {
          updateCurveData(cp.id.color, lastCarData.get, currentCarData)
        }
      })
    } else {
      cps.data.foreach(cp => {
        val oldCd = cars.get(cp.id.color)
        val cd = CarData(oldCps.data.filter(_.id.color == cp.id.color).head, cp, bot)
        if (cp.id.color != bot.myCar.color) {
          cars(cp.id.color).update(cd)
        }
        val lastCarData = cars.get(cp.id.color)
        if (lastCarData.isDefined) {
          updateCurveData(cp.id.color, lastCarData.get, cd)
        }
      })
    }

    oldCps = cps
    checkPendingUpdates()
  }

  def enemyInfront(): Option[CarData] = {
    cars.values.filter(car => car.lane.endLaneIndex == bot.currentLane && car.distance >= 0 && car.distance < bot.carLength * 2).headOption
  }

  def enemyInRammingDistance(): Boolean = {
    if (bot.course.get(bot.currentPieceIndex).get.straight) {
      val curveDist = SpeedLimiter.distanceToNextSpeedLimit(bot.currentDistance, bot.trackLength)._1
      cars.values.filter(car => car.lane.endLaneIndex == bot.currentLane && car.distance >= 0 && car.distance < bot.carLength * 1).exists { c =>
        bot.currentSpeed > c.speed && curveDist / bot.currentSpeed > c.distance / (bot.currentSpeed - c.speed)
      }
    } else {
      false
    }
    false

  }
}