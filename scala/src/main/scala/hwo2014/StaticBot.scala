package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{ BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter }
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.collection.immutable.List
import scala.collection.mutable.Map
import scala.collection.mutable.ListBuffer

class StaticBot(val host: String, val port: Int, val botName: String, val botKey: String) extends Bot {
  override def initialization(initMessage: GameInit) {}
  override def handlePositions(cps: CarPositions) {
    throttle = 0.5f
  }
  override def handleTurbo() {}
  override def spawn {}
  override def finish {}
}
