package hwo2014

import scala.collection.mutable.ListBuffer
import scala.concurrent.SyncVar
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction
import scala.collection.mutable.SortedSet
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator

object BrakeAnalyzer {

  val brakeDecelerations = SortedSet[SpeedAccPair]()

  private var brakeFunction: PolynomialSplineFunction = null

  def addBrakingData(speed: Double, deceleration: Double) = {
    if (deceleration < 0) {
      brakeDecelerations += (SpeedAccPair(speed, deceleration))
    }
  }

  def interpolate() = {
    if (brakeDecelerations.size > 2) {
      val start = System.nanoTime()
      brakeFunction = (new SplineInterpolator()).interpolate(
        brakeDecelerations.map(_.speed).toArray,
        brakeDecelerations.map(_.acceleration).toArray)
      val duration = (System.nanoTime() - start) / 1000000
      Logger.logLine(s"interpolation time $duration ${brakeDecelerations.size} datapoints")
    } else {
      Logger.logLine("brake dec size: " + brakeDecelerations.size)
    }
  }

  def getBrakeValue(speed: Double): Double = {
    if (brakeFunction == null) {
      Logger.logLine("should not be here: brake")
      -0.2f
      //      val nearestDecelerationSpeed = decelerations.filter(_._1 <= speed).map(_._1).max
      //      decelerations.get(nearestDecelerationSpeed).get
    } else {
      val knots = brakeFunction.getKnots

      val d = if (speed < knots(0)) {
        brakeFunction.value(knots(0))
      } else if (speed > knots(knots.length - 1)) {
        brakeFunction.value(knots(knots.length - 1))
      } else {
        brakeFunction.value(speed)
      }
      if (brakeDecelerations.size < 30) {
        math.min(d, -0.08)
      } else {
        d
      }
    }
  }

  // True if need to brake
  private def checkLimit(lane: Int, speed: Double, curveData: (Double, Curve), switchDecisions: List[(Double, Option[LaneChange.Value])], bot: Bot): Boolean = {
    var distanceLeft = curveData._1
    val curve = curveData._2

    val laneResult = switchDecisions.filter(_._1 < (bot.currentDistance + curveData._1) % bot.trackLength).map(i => i._2 match {
      case Some(LaneChange.Left) => -1
      case Some(LaneChange.Right) => 1
      case _ => 0
    }).fold(lane)((acc, i) => acc + i)

    val cappedLane = math.min(laneResult, curve.lanes.size - 1)
    val finalLane = math.max(0, cappedLane)
    val curveLane = curve.lanes.find(_.index == finalLane).get

    val targetSpeed = SpeedAnalyzer.getSafeSpeed(curveLane.laneRadius)

    var tmpSpeed = speed

    // Iterate single ticks
    // each tick decreases speed and consumes distanceLeft
    // When distance is
    val brakeLogic = ListBuffer[String]()
    var index = 1
    while (distanceLeft > 0 && tmpSpeed > targetSpeed && tmpSpeed > 2) {
      // Deceleration is negative
      val deceleration = getBrakeValue(tmpSpeed)
      distanceLeft -= tmpSpeed
      brakeLogic.append(s"$index: d${formatD(distanceLeft)}, s${formatD(tmpSpeed)}, b${formatD(deceleration)}")
      tmpSpeed += deceleration
      index += 1
    }

    if (distanceLeft <= 0 || tmpSpeed > targetSpeed) {
      Logger.logLine(s"distanceLeft: ${formatD(curveData._1)}, ci: ${curve.index}, pos: ${curve.startPos}, targetSpeed: ${formatD(targetSpeed)}" + brakeLogic.mkString(", "))
    }

    // If distanceLeft > 0 -> no need to brake just yet
    // if tmpSpeed > targetSpeed -> brake

    // Adding distaceLeft check changed overspeeds from 30 to 18
    //tmpSpeed > targetSpeed
    distanceLeft <= 0 || tmpSpeed > targetSpeed

  }

  def formatD(value: Double): String = {
    "%.3f".format(value)
  }

  def needToBrake(currentSpeed: Double, currentDistance: Double, currentLane: Int, trackLength: Double, bot: LeBot): Boolean = {
    // TODO add lane change logic afterwards!
    val switchDecisions = for (i <- bot.currentPieceIndex to (bot.currentPieceIndex + 10) % bot.course.size) yield (bot.course(i).absolutePosition, LaneHelper.shouldChangeLane(i, bot.competitorAnalyzer, bot.ramming))
    // List[(distance: Double, curve: Curve)]
    val limits = SpeedLimiter.getDistancesAndLimits(currentDistance, trackLength)
    val brake = limits.exists(l => checkLimit(currentLane, currentSpeed, l, switchDecisions.toList, bot))
    if (bot.ramming && bot.competitorAnalyzer.enemyInRammingDistance) {
      println(bot.gameTick + " Still ramming, no brake")
      false
    }
    if (brake) {
      Logger.logLine(s"needToBrake: $brake, currentDistance: ${formatD(currentDistance)}, currentSpeed: ${formatD(currentSpeed)}, cpi: ${bot.currentPieceIndex}")
    }
    brake
  }

}