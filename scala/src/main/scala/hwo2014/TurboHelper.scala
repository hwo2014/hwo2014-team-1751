package hwo2014

object TurboHelper {

  var straights = Map[Int, Double]()
  var course = Map[Int, CoursePiece]()
  
  def init(straightsMap: Map[Int, Double], courseMap: Map[Int, CoursePiece]) {
    straights = straightsMap
    course = courseMap
  }
  
  def shouldUseTurbo(currentPieceIndex: Int): Boolean = {
    val fastestPieces = straights.filter(_._2 == straights.map(_._2).max).map(_._1).toList
    if (fastestPieces.contains(currentPieceIndex) || fastestPieces.contains(currentPieceIndex + 1)) {
      Logger.logLine(s"Use turbo! CurrentPiece or next piece is on fastest pieces list")
      true
    } else {
      var index = (currentPieceIndex + 1) % course.size
      var straightDistance = 0d
      while (course.get(index).get.straight) {
        straightDistance += course.get(index).get.length
        index = (index + 1) % course.size
      }
      if (straightDistance > 400) {
        Logger.logLine(s"Use turbo! Straight 400+ ahead")
        true
      } else {
        false
      }
    }
  }
}