package hwo2014

import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{ BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter }
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.collection.mutable.Map
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator
import scala.collection.mutable.Queue
import scala.collection.mutable.ListBuffer

trait Bot {
  val host: String
  val port: Int
  val botName: String
  val botKey: String

  implicit val formats = new DefaultFormats {}

  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))

  val durations = new ListBuffer[Long]()

  val course = Map[Int, CoursePiece]()
  var trackLength: Double = 0

  var myCar: CarId = CarId("", "")
  var carLength: Double = 0

  var currentAngle: Double = 0
  var throttle = 1f
  var isCrashed = false
  var lap = 1
  var crashes = 0
  var currentPieceIndex: Int = 0
  var previousPieceIndex: Int = 0
  var currentPiece: Option[CoursePiece] = None
  var turboEnabledTicks = 0
  var turboFactor = 1d
  var gameTick: Int = 0
  var currentLane = 0

  val interpolateInterval = 20

  var previousSpeed: Double = 0
  var currentSpeed: Double = 0

  var currentDistance: Double = 0
  var previousDistance: Double = 0

  var currentCP: Option[CarPosition] = None
  var previousCP: Option[CarPosition] = None

  var lanes = List[Lane]()
  val straights = Map[Int, Double]()

  var turbo: Option[TurboData] = None

  val competitorAnalyzer = new CompetitorAnalyzer(this)

  def formatD(value: Double): String = {
    "%.3f".format(value)
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

  def initialization(init: GameInit) = {
    var trackDistance = 0d

    init.data.race.track.pieces.zipWithIndex.foreach(tuple => {
      val p = new CoursePiece(tuple._1, trackDistance)
      course(tuple._2) = p
      trackDistance += p.length
    })

    lanes = init.data.race.track.lanes.map(l => {
      new Lane(l.distanceFromCenter, l.index)
    })

    @tailrec def getStraightLength(index: Int, acc: Double): Double = {
      course.get(index % course.size) match {
        case Some(cp) if (cp.straight) => (getStraightLength(index + 1, acc + cp.length))
        case _ => acc
      }
    }

    course.foreach(tuple => straights.put(tuple._1, getStraightLength(tuple._1, 0)))

    trackLength = course.foldLeft(0d)((a, i) => a + i._2.length)
    carLength = init.data.race.cars.filter(_.id == myCar).head.dimensions.length
  }

  def handlePositions(cps: CarPositions) = {
    gameTick = cps.gameTick.getOrElse(0)

    val cp = cps.data.filter(_.id.color == myCar.color).head
    currentPiece = Some(course(cp.piecePosition.pieceIndex))

    previousCP = currentCP
    currentCP = Some(cp)
    previousPieceIndex = currentPieceIndex
    currentPieceIndex = cp.piecePosition.pieceIndex

    if (turboEnabledTicks > 0) {
      turboEnabledTicks -= 1
    }

    previousDistance = currentDistance
    currentDistance = currentPiece.get.absolutePosition + cp.piecePosition.inPieceDistance
    previousSpeed = currentSpeed
    currentSpeed = calculateSpeed(currentCP, previousCP)
    currentLane = cp.piecePosition.lane.endLaneIndex

    competitorAnalyzer.update(cps)
  }

  def handleTurbo() = {
    if (turbo.isDefined) {
      Logger.logLine("Using TURBO")
      val t = turbo.get
      turboEnabledTicks = t.turboDurationTicks
      turboFactor = t.turboFactor
      turbo = None
      send(MsgWrapper("turbo", "Turbo is super effective"))
    } else {
      Logger.logLine("Tried to use turbo, but none available.")
    }
  }
  def spawn
  def finish
  def crashed = {}

  def createAndStart(track: String, passwd: String, drivers: Int) {
    send(MsgWrapper("createRace",
      CreateRace(BotId(botName, botKey), track, passwd, drivers)))
    play
  }

  def joinCreated(track: String, passwd: String, drivers: Int) {
    send(MsgWrapper("joinRace",
      CreateRace(BotId(botName, botKey), track, passwd, drivers)))
    play
  }

  def start {
    send(MsgWrapper("join", Join(botName, botKey)))
    play
  }

  def calculateSpeed(currentPosition: Option[CarPosition], previousPosition: Option[CarPosition]): Double = {
    val speed = if (currentPosition.isEmpty && previousPosition.isEmpty) {
      0
    } else if (previousPosition.isEmpty) {
      currentPosition.get.piecePosition.inPieceDistance
    } else if (currentPosition.get.piecePosition.pieceIndex == previousPosition.get.piecePosition.pieceIndex) {
      currentPosition.get.piecePosition.inPieceDistance - previousPosition.get.piecePosition.inPieceDistance
    } else {
      val prevPiece = course.get(previousPosition.get.piecePosition.pieceIndex).get
      val prevPieceLaneLength = if (prevPiece.piece.length.isDefined) {
        prevPiece.piece.length.get
      } else {
        val lane = currentPosition.get.piecePosition.lane.startLaneIndex
        val laneOffset = lanes.find(_.index == lane).get.distanceFromCenter
        val r = prevPiece.radius(laneOffset).get
        val a = math.abs(prevPiece.piece.angle.get)
        math.Pi * 2 * r * a / 360
      }
      currentPosition.get.piecePosition.inPieceDistance + prevPieceLaneLength - previousPosition.get.piecePosition.inPieceDistance
    }
    if (speed > 80) {
      Logger.logLine(s"currentPosition: " + currentPosition.toString)
      Logger.logLine(s"previousPosition: " + previousPosition.toString)
    }
    
    speed
  }

  @tailrec private def play {
    val line = reader.readLine()
    if (line != null) {
      try {
        Serialization.read[MsgWrapper](line) match {
          case MsgWrapper("gameInit", data) =>
            Logger.logLine(s"\nReceived the following init message\n$line\n\n")

            val init: GameInit = Serialization.read[GameInit](line)
            initialization(init)

          case MsgWrapper("yourCar", data) =>
            val yourCar: YourCar = Serialization.read[YourCar](line)
            myCar = yourCar.data
            Logger.logLine(s"We are $myCar")
          case MsgWrapper("carPositions", data) =>
            val start = System.nanoTime()
            val cps: CarPositions = Serialization.read[CarPositions](line)
            handlePositions(cps)
            durations += System.nanoTime() - start
          case MsgWrapper(msgType, _) =>
            Logger.logLine("Received: " + msgType + " #####################################################")
            Logger.logLine(s"\n$line\n")
            if (msgType == "spawn") {
              val spawnMessage = Serialization.read[Spawn](line)
              if (spawnMessage.data.color == myCar.color) {
                throttle = 1f
                isCrashed = false
                spawn
              }
            } else if (msgType == "gameStart") {
              currentAngle = 0
              throttle = 1f
              isCrashed = false
              lap = 1
              crashes = 0
              currentPieceIndex = 0
              previousPieceIndex = 0
              currentPiece = None
              turboEnabledTicks = 0
              turboFactor = 1d
              gameTick = 0
              currentLane = 0
            } else if (msgType == "join") {
              Logger.logLine(line)
            } else if (msgType == "crash") {
              val crashMessage = Serialization.read[Crash](line)
              if (crashMessage.data.color == myCar.color) {
                isCrashed = true
                crashes += 1
                crashed
              } 
              competitorAnalyzer.crashed(crashMessage.data.color)
            } else if (msgType == "lapFinished") {
              if (line.contains(myCar.color))
                lap += 1
            } else if (msgType == "turboAvailable") {
              val turboMsg = Serialization.read[TurboAvailable](line)
              turbo = Some(turboMsg.data)
            } else if (msgType == "turboEnd") {
              val turboEndMessage = Serialization.read[TurboEnd](line)
              if (turboEndMessage.data.color == myCar.color) {
                turboEnabledTicks = 0
                turboFactor = 1
              }
            } else if (msgType == "finish") {
              finish
            } else if (msgType == "error") {
              Logger.logLine(line)
            }
          case _ =>
            Logger.logLine(s"No within the protocol \n$line")
        }
      } catch {
        case e: Exception => {
          Logger.logLine(s"Something bad happened. Lets just close our eyes and pretend nothing happened\n$line")
          Logger.logLine(e.getMessage() )
          e.printStackTrace()
        }
      }

      send(MsgWrapper("throttle", throttle))
      play
    } else {
      Logger.logLine("Ending, server sent back nothing")
      if (durations.size > 0) {
        val averageDuration = durations.fold(0l)((acc, i) => acc + i) / durations.size
        val minDuration = durations.min
        val maxDuration = durations.max
        Logger.logLine(s"Min, Max, Average\n$minDuration, $maxDuration, $averageDuration")
      }

      Logger.messages.foreach(println(_))
    }
  }

}