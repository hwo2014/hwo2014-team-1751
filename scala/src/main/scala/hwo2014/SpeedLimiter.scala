package hwo2014

import scala.collection.mutable.LinkedList

case class CurveLane(index: Int, laneOffset: Int, laneRadius: Int, var manualSpeedLimit: Option[Double])
case class Curve(index: Int, var endIndex: Int, startPos: Double, var endPos: Double, angle: Double, pieceRadius: Int, lanes: List[CurveLane], switch: Boolean)

object SpeedLimiter {

  var curves = Seq[Curve]()

  def addCurve(curve: Curve) = {
    // Note angle doesn't have to be same
    if (curves.size > 0 && curves.last.pieceRadius == curve.pieceRadius && curves.last.endIndex == curve.index - 1 && !curve.switch) {
      curves.last.endPos = curve.endPos
      curves.last.endIndex = curve.index
    } else {
      //curves.append(curve)
      curves = curves :+ curve
    }
  }

  def distanceToNextSpeedLimit(currentDistance: Double, trackLength: Double): (Double, Int) = {
    val nextLimit = curves.filter(c => c.startPos <= currentDistance && c.endPos >= currentDistance).headOption

    if (nextLimit.isDefined) {
      (nextLimit.get.startPos - currentDistance, nextLimit.get.index)
    } else {
      val firstLimitIndex = curves.map(_.index).min
      val firstLimit = curves.find(_.index == firstLimitIndex).get
      (firstLimit.startPos + trackLength - currentDistance, firstLimit.index)
    }
  }

  /*def speedLimitOnCurve(index: Int, lane: Int): Double = {
	val laneRadius = curves.find(_.index == index).get.lanes.find(l => l.index == lane).get.laneRadius
    SpeedAnalyzer.getSafeSpeed(laneRadius)
  }*/

  def speedLimitOnPosition(pos: Double, laneIndex: Int): Double = {
    var curvePiece = curves.find(c => c.startPos <= pos && c.endPos >= pos)
    if (curvePiece.isEmpty) {
      val nearestPos = curves.map(_.endPos).filter(_ < pos).max
      curvePiece = curves.find(c => c.endPos == nearestPos)
    }
    val lane = curvePiece.get.lanes.find(l => l.index == laneIndex).get
    val laneRadius = lane.laneRadius
    if (lane.manualSpeedLimit.isDefined) {
      lane.manualSpeedLimit.get
    } else {
      SpeedAnalyzer.getSafeSpeed(laneRadius)
    }
  }

  def getDistancesAndLimits(currentDistance: Double, trackLength: Double, range: Double = 600): List[(Double, Curve)] = {
    val curvesOnRange = curves.filter(c => c.startPos <= currentDistance + range && c.startPos >= currentDistance)
    val limitsFromTheRestOfTheLap = curvesOnRange.toList.map(c => (c.startPos - currentDistance, c))
    if (currentDistance + range > trackLength) {
      val curvesForTheNextLap = curves.filter(c => c.startPos <= currentDistance + range - trackLength)
      val limitsForTheNextLap = curvesForTheNextLap.toList.map(c => (c.startPos + trackLength - currentDistance, c))
      limitsFromTheRestOfTheLap ::: limitsForTheNextLap
    } else {
      limitsFromTheRestOfTheLap
    }
  }

  def curveIsEnding(currentDistance: Double, currentSpeed: Double, currentLane: Int, currentAngle: Double, changeOfAngle: Double): Boolean = {
    val currentLimit = curves.filter(c => c.startPos <= currentDistance && c.endPos >= currentDistance).headOption
    if (currentLimit.isEmpty) {
      true
    } else {
      val laneLength = currentLimit.get.angle * currentLimit.get.lanes.find(_.index == currentLane).get.laneRadius * math.Pi / 180

      val curveLeft = currentLimit.get.endPos - currentDistance
      val ticksLeftOnCurve = curveLeft / currentSpeed

      // 8 && 30, 4 && 45 crashes 4 times on France
      // 5&&30, 3&&45 no crashes and 8:07 
      // 8&&30&&1, 4&&45&&1 changeOfAngle 8:18
      //8&&30&&1, 4&&45&&1 abs.changeOfAngle 8:12

      if (ticksLeftOnCurve < 8 && math.abs(currentAngle) < 10 && changeOfAngle < 1) {
        true
      } else if (ticksLeftOnCurve < 4 && math.abs(currentAngle) < 45 && changeOfAngle < 1.5) {
        true
      } else {
        false
      }
    }
  }

  def useTurboAtEndOfCurve(currentDistance: Double, currentSpeed: Double, currentLane: Int): Boolean = {
    val currentLimit = curves.filter(c => c.startPos <= currentDistance && c.endPos >= currentDistance).headOption
    if (currentLimit.isEmpty) {
      true
    } else {
      val laneLength = currentLimit.get.angle * currentLimit.get.lanes.find(_.index == currentLane).get.laneRadius * math.Pi / 180
      val curveLeft = currentLimit.get.endPos - currentDistance
      val ticksLeftOnCurve = curveLeft / currentSpeed
      ticksLeftOnCurve < 3
    }

  }

}