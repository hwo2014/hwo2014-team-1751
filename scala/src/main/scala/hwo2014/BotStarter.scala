package hwo2014

object BotStarter extends App {
  args.toList match {
    case "create" :: trackName :: passWord :: driversStr :: _ =>
      //val bot = new NoobBot("hakkinen.helloworldopen.com", 8091, "JohnRambo", "WEQCyMqJkzW2EQ")
      val bot = new LeBot("hakkinen.helloworldopen.com", 8091, "Angus MacGyver", "WEQCyMqJkzW2EQ")
      bot.createAndStart(trackName, passWord, Integer.parseInt(driversStr))
    case "join" :: driverName :: trackName :: passWord :: driversStr :: _ =>
      val bot = new NoobBot("hakkinen.helloworldopen.com", 8091, driverName, "WEQCyMqJkzW2EQ")
      bot.joinCreated(trackName, passWord, Integer.parseInt(driversStr))
    case "join2" :: driverName :: trackName :: passWord :: driversStr :: _ =>
      val bot = new LeBot("hakkinen.helloworldopen.com", 8091, driverName, "WEQCyMqJkzW2EQ")
      bot.joinCreated(trackName, passWord, Integer.parseInt(driversStr))
    case hostName :: port :: botName :: botKey :: _ =>
      val bot = new LeBot(hostName, Integer.parseInt(port), botName, botKey)
      bot.start
    case _ => {
      val bot = new LeBot("hakkinen.helloworldopen.com", 8091, "Rooster", "WEQCyMqJkzW2EQ")
      bot.start
    }
  }
}