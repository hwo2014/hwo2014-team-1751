package hwo2014

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.SortedSet
import scala.concurrent.SyncVar
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator

object ThrottleAnalyzer {

  private val throttleAccelerations = SortedSet[SpeedAccPair]()

  private var throttleFunction: PolynomialSplineFunction = null

  def addThrottlingData(speed: Double, acceleration: Double) = {
    if (acceleration > 0) {
      throttleAccelerations += (SpeedAccPair(speed, acceleration))
    }
  }

  def interpolate() = {
    if (throttleAccelerations.size > 2) {
      val start = System.nanoTime()
      throttleFunction = (new SplineInterpolator()).interpolate(
        throttleAccelerations.map(_.speed).toArray,
        throttleAccelerations.map(_.acceleration).toArray)
      val duration = (System.nanoTime() - start) / 1000000
      Logger.logLine(s"interpolation time $duration ${throttleAccelerations.size} datapoints")
    } else {
      Logger.logLine("throttle acc size: " + throttleAccelerations.size)
    }
  }

  def getThrottleValue(speed: Double): Double = {
    if (throttleFunction == null) {
      Logger.logLine("should not be here: throttle")
      1.0f
    } else {
      val knots = throttleFunction.getKnots

      if (speed < knots(0)) {
        throttleFunction.value(knots(0))
      } else if (speed > knots(knots.length - 1)) {
        throttleFunction.value(knots(knots.length - 1))
      } else {
        throttleFunction.value(speed)
      }
    }
  }
}