package hwo2014

import scala.collection.mutable.ListBuffer

object LaneChange extends Enumeration {
  val Left, Right = Value
}

object LaneHelper {
  // Should know which lane is in inner curve most between switch
  // Should know if trying to overtake someone

  val switchesToInnerLane = ListBuffer[(Int, Option[LaneChange.Value])]()

  def init(course: Map[Int, CoursePiece]) = {
    course.filter(c => c._2.piece.switch.isDefined && c._2.piece.switch.get).foreach { c =>
      var index = (c._1 + 1) % course.size
      // This doesn't calculate track lengths
      // Instead this calculates distance of track turning into direction
      // So if leftDistance is greater than right, ai should take left lane
      var leftDistance = 0d
      var rightDistance = 0d
      var cpiece = course.get(index).get
      while (!cpiece.piece.switch.isDefined || !cpiece.piece.switch.get) {
        if (cpiece.piece.angle.isDefined && cpiece.piece.angle.get > 0) {
          rightDistance += cpiece.length
        } else if (cpiece.piece.angle.isDefined && cpiece.piece.angle.get < 0) {
          leftDistance += cpiece.length
        }
        index = (index + 1) % course.size
        cpiece = course.get(index).get
      }

      if (leftDistance == rightDistance) {
        switchesToInnerLane.append((c._1, None))
      } else if (leftDistance > rightDistance) {
        switchesToInnerLane.append((c._1, Some(LaneChange.Left)))
      } else {
        switchesToInnerLane.append((c._1, Some(LaneChange.Right)))
      }

    }
  }

  private def switchToInnerLane(switchIndex: Int): Option[LaneChange.Value] = {
    val direction = switchesToInnerLane.find(_._1 == switchIndex)
    if (direction.isEmpty) {
      None
    } else {
      direction.get._2
    }
  }

  def shouldChangeLane(switchIndex: Int, competitorAnalyzer: CompetitorAnalyzer, isRamming: Boolean): Option[LaneChange.Value] = {
    // TODO implement overtakes!
    val s = switchToInnerLane(switchIndex)
    if (competitorAnalyzer.enemyInRammingDistance) {
      None
    }
    else if (competitorAnalyzer.enemyInfront.isDefined) {
      s.map(i => {
        i match {
          case LaneChange.Left => LaneChange.Right
          case _ => LaneChange.Left
        }
      })
    } else {
      s
    }
    

  }

}