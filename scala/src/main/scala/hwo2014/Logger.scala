package hwo2014

import scala.collection.mutable.ListBuffer

object Logger {
  val dev = true

  val messages = ListBuffer[String]()

  def logLine(message: String) {
    if (dev) {
      messages += message
      //println(message)
    }
  }
}