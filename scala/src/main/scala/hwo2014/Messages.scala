package hwo2014

import org.json4s.JsonAST.JValue
import org.json4s.DefaultFormats
import org.json4s.Extraction

case class Join(name: String, key: String)
case class BotId(name: String, key: String)
case class CreateRace(botId: BotId, trackName: String, password: String, carCount: Int)

case class YourCar(msgType: String, data: CarId, gameId: String)

case class Crash(msgType: String, data: CarId, gameId: String, gameTick: Int)
case class Spawn(msgType: String, data: CarId, gameId: String, gameTick: Int)
case class TurboEnd(msgType: String, data: CarId, gameTick: Int)

case class CoursePiece(piece: Piece, absolutePosition: Double) {
  val length: Double = {
    piece.length.getOrElse(
      piece.radius.get * math.abs(piece.angle.get) / 180 * math.Pi)
  }
  def radius(laneOffset: Int): Option[Int] = {
    // A positive value tells that the lanes is to the right from the center line 
    // while a negative value indicates a lane to the left from the center.

    // right means smaller radius on right turn
    // 		angle > 0 -> radius -offset
    // right means greater radius on left turn
    // 		angle < 0 -> radius + offset
    if (piece.angle.isEmpty || piece.radius.isEmpty) {
      None
    } else if (piece.angle.get > 0) {
      Some(piece.radius.get - laneOffset)
    } else {
      Some(piece.radius.get + laneOffset)
    }
  }

  val straight: Boolean = piece.length.isDefined

}

case class Piece(length: Option[Double], switch: Option[Boolean], radius: Option[Int], angle: Option[Double]) {
  def realLength: Double = {
    length.getOrElse(
      radius.get * math.abs(angle.get) / 180 * math.Pi)
  }
}

case class Lane(distanceFromCenter: Int, index: Int)
case class Coordinates(x: Double, y: Double)
case class StartingPoint(position: Coordinates, angle: Double)
case class Track(id: String, name: String, pieces: List[Piece], lanes: List[Lane], startingPoint: StartingPoint)
case class RaceSession(laps: Option[Int], maxLapTimeMs: Option[Long], quickRace: Option[Boolean])
case class CarDimensions(length: Double, width: Double, guideFlagPosition: Double)
case class Car(id: CarId, dimensions: CarDimensions)
case class Race(track: Track, cars: List[Car], raceSession: Option[RaceSession])
case class RaceData(race: Race)
case class GameInit(msgType: String, data: RaceData)

case class CarId(name: String, color: String)
case class LanePosition(startLaneIndex: Int, endLaneIndex: Int)
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: LanePosition, lap: Int)
case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition)
case class CarPositions(msgType: String, data: List[CarPosition], gameId: String, gameTick: Option[Int])
case class TurboData(turboDurationMilliseconds: Double, turboDurationTicks: Int, turboFactor: Double)
case class TurboAvailable(msgType: String, data: TurboData)

case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats {}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
