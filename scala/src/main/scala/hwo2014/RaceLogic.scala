package hwo2014

class RaceLogic(bot: LeBot) {

  def think(): (Float, Boolean, Option[LaneChange.Value]) = {

    var throttle = 1f
    var useTurbo = false
    var switchLane: Option[LaneChange.Value] = None

    val changeOfAngle = math.abs(bot.currentRealAngle) - math.abs(bot.previousRealAngle)

    // If speed is too high, brake
    if (BrakeAnalyzer.needToBrake(bot.currentSpeed, bot.currentDistance, bot.currentLane, bot.trackLength, bot)) {
      bot.changeMode(BehaviourMode.NearCurve)
      throttle = 0
    } // If curve is ending, start accelerating on next straight
    else if (!bot.currentPiece.get.straight && SpeedLimiter.curveIsEnding(bot.currentDistance, bot.currentSpeed, bot.currentLane, bot.currentAngle, changeOfAngle)) {
      throttle = 1
		// No turbo on curve
		/*useTurbo = SpeedLimiter.useTurboAtEndOfCurve(bot.currentDistance, bot.currentSpeed, bot.currentLane) &&
			TurboHelper.shouldUseTurbo(bot.currentPieceIndex+1)*/
	}
    // If on curve, maintain safe speed
    else if (!bot.currentPiece.get.straight) {

      // Maintain speed
      // Get estimates for speed with throttle 1 and throttle 0
      // estimate throttle value
      bot.changeMode(BehaviourMode.OnCurve)
      //val safeSpeed = SpeedLimiter.speedLimitOnCurve( bot.currentPieceIndex, bot.currentLane)
      val safeSpeed = SpeedLimiter.speedLimitOnPosition(bot.currentDistance, bot.currentLane)

      val throttleSpeed = bot.currentSpeed + ThrottleAnalyzer.getThrottleValue(bot.currentSpeed)
      val brakeSpeed = bot.currentSpeed + BrakeAnalyzer.getBrakeValue(bot.currentSpeed)

      if (throttleSpeed <= safeSpeed) {
        throttle = 1
      } else if (brakeSpeed >= safeSpeed) {
        throttle = 0
      } else {
        val range = throttleSpeed - brakeSpeed
        val posAtRange = bot.currentSpeed - brakeSpeed
        throttle = (posAtRange / range).toFloat
      }
    } // If on straight road, full throttle
    else {
      if (bot.turbo.isDefined && TurboHelper.shouldUseTurbo(bot.currentPieceIndex)) {
        useTurbo = true
      }
      bot.changeMode(BehaviourMode.StraightRoad)
      throttle = 1
    }

    if (bot.competitorAnalyzer.enemyInRammingDistance){
      println("TRYING TO RAM")
      throttle = 1
      if (bot.turbo.isDefined && TurboHelper.shouldUseTurbo(bot.currentPieceIndex)) {
        useTurbo = true
      }
      if (bot.ramming == false) {
        bot.ramming = true
        bot.rammingCounter = 10
      }
      
    }
    
    if (bot.previousPieceIndex != bot.currentPieceIndex) {
      val nextPieceIndex = (bot.currentPieceIndex + 1) % bot.course.size
      val nextPiece = bot.course.get(nextPieceIndex).get
      if (nextPiece.piece.switch.isDefined && nextPiece.piece.switch.get) {
        switchLane = LaneHelper.shouldChangeLane(nextPieceIndex, bot.competitorAnalyzer, bot.ramming)
      }
    }

    if (changeOfAngle > 2.5) {
      throttle = 0
    } 
    
    // TODO turbo-logic
    // If long enough straight road is ahead, use
    // UNLESS there is a car infront too close and there isn't switch coming 
    // TODO react to other cars

    (throttle, useTurbo, switchLane)
  }

}