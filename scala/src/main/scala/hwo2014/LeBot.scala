package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.collection.immutable.List
import scala.collection.mutable.Map
import scala.collection.mutable.Set
import scala.collection.mutable.ListBuffer
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator
import scala.collection.mutable.SortedSet

object State extends Enumeration {
  val QualifyingTestAcc, QualifyingTestDec, Qualifying, Race = Value
}

object BehaviourMode extends Enumeration {
  val StraightRoad, NearCurve, OnCurve = Value
}

case class SpeedAccPair(speed: Double, acceleration: Double) extends Ordered[SpeedAccPair] {
  def compare(that: SpeedAccPair) = this.speed.compare(that.speed)
}
class LeBot(val host: String, val port: Int, val botName: String, val botKey: String) extends Bot {

  var state = State.QualifyingTestAcc
  var mode = BehaviourMode.StraightRoad
  var raceLogic = new RaceLogic(this)

  var currentRealAngle: Double = 0
  var previousRealAngle: Double = 0

  var bumped = false
  var bumpedCounter = 0

  var ramming = false
  var rammingCounter = 0

  override def initialization(init: GameInit) = {
    super.initialization(init)

    course.keySet.toList.sorted.foreach { key =>
      val c = (key, course.get(key).get)
      if (!c._2.straight) {
        val piece = c._2.piece
        val pieceRadius = piece.radius.get
        val angle = piece.angle.get

        val laneRadiuses = lanes.map(l => (l.index, l.distanceFromCenter, c._2.radius(l.distanceFromCenter).get))
        val curveLanes = laneRadiuses.map(lr => CurveLane(lr._1, lr._2, lr._3, None))
        val curveEnd = c._2.absolutePosition + math.abs(angle) * pieceRadius * Math.PI / 180
        val curve = Curve(c._1, c._1, c._2.absolutePosition, curveEnd, angle, pieceRadius, curveLanes, c._2.piece.switch.getOrElse(false))
        SpeedLimiter.addCurve(curve)
      }
    }

    Logger.logLine("SpeedLimitedAreas")
    SpeedLimiter.curves.toList.sortBy(_.index).map { c =>
      //case class Curve(index: Int, startPos: Double, var endPos: Double, angle: Double, pieceRadius: Int, lanes: List[CurveLane])
      Logger.logLine(s"  ${formatD(c.startPos)}  -  ${formatD(c.endPos)}, ${formatD(c.pieceRadius)}")
    }

    LaneHelper.init(course.toMap)
    TurboHelper.init(straights.toMap, course.toMap)
    throttle = 1

  }

  private def handleDecisions(throttleVal: Float, useTurbo: Boolean, switchLane: Option[LaneChange.Value]) = {
    throttle = throttleVal
    if (useTurbo) {
      handleTurbo
    }
    if (switchLane.isDefined) {
      if (switchLane.get == LaneChange.Left) {
        send(MsgWrapper("switchLane", "Left"))
      } else {
        send(MsgWrapper("switchLane", "Right"))
      }
    }
  }

  override def handlePositions(cps: CarPositions) = {
    super.handlePositions(cps)
    val cp = cps.data.filter(_.id.color == myCar.color).head

    previousRealAngle = currentRealAngle
    currentRealAngle = cp.angle

    if (!isCrashed && throttle == 1 && turboEnabledTicks == 0) {
      ThrottleAnalyzer.addThrottlingData(currentSpeed, currentSpeed - previousSpeed)
    }
    if (!isCrashed && throttle == 0 && currentSpeed - previousSpeed < 0 && currentAngle < 20) {
      BrakeAnalyzer.addBrakingData(currentSpeed, currentSpeed - previousSpeed)
    }
    if (throttle == 0 && currentSpeed > previousSpeed) {
      bumped = true
      bumpedCounter = 10
    } else {
      bumpedCounter -= 1
      if (bumpedCounter == 0) {
        bumped = false
      }
    }
    if (ramming && throttle == 1 && currentSpeed < previousSpeed) {
      ramming = false
      throttle = 0
    }
    if (ramming) {
      rammingCounter -= 1
      if (rammingCounter == 0) {
        ramming = false
      }
    }

    if (isCrashed && bumped) {
      bumped = false
      bumpedCounter = 0
    }

    state match {
      case State.QualifyingTestAcc => {
        throttle = 1

        if (gameTick % 10 == 2 || gameTick % 10 == 3 || gameTick % 10 == 4) {
          throttle = 0
        }

        if (currentSpeed - previousSpeed < 0) {
          BrakeAnalyzer.addBrakingData(currentSpeed, currentSpeed - previousSpeed)
        } else {
          ThrottleAnalyzer.addThrottlingData(currentSpeed, currentSpeed - previousSpeed)
        }
        QualifyingHelper.ticksToAccelerate -= 1
        if (QualifyingHelper.ticksToAccelerate == 0) {
          ThrottleAnalyzer.interpolate
          BrakeAnalyzer.interpolate
          state = State.Qualifying
          throttle = 0
        }

      }
      case State.QualifyingTestDec => {
        throttle = 0
        BrakeAnalyzer.addBrakingData(currentSpeed, currentSpeed - previousSpeed)
        QualifyingHelper.ticksToDecelerate -= 1
        if (QualifyingHelper.ticksToDecelerate == 0) {
          BrakeAnalyzer.interpolate
          state = State.QualifyingTestAcc
        }

      }
      case State.Qualifying => {
        handleInterpolation()
        val (throttleVal, turbo, laneChange) = raceLogic.think
        handleDecisions(throttleVal, turbo, laneChange)
      }
      case State.Race => {
        handleInterpolation()
        val (throttleVal, turbo, laneChange) = raceLogic.think
        handleDecisions(throttleVal, turbo, laneChange)
      }
    }

    throttle = math.min(1, throttle)
    throttle = math.max(0, throttle)

    Logger.logLine(s"Tick: ${gameTick}, pos: ${formatD(currentDistance)}, LastSpeed: ${formatD(currentSpeed)}, Next throttle: ${formatD(throttle)}")
    if (gameTick % 10 == 0) {
      logStuff
    }
  }

  override def finish() = {
    Logger.logLine(s"Crashed ${crashes} times")
    Logger.logLine(s"MagicVar ${SpeedAnalyzer.magicVar}")
    Logger.logLine(s"Learned speeds")

    SpeedLimiter.curves.flatMap(c => c.lanes).filter(_.manualSpeedLimit.isDefined).foreach { limit =>
      Logger.logLine(s"  Index ${limit.index}, radius: ${limit.laneRadius}, speed with latest magicVar: ${formatD(SpeedAnalyzer.getSafeSpeed(limit.laneRadius))}, manualLimit: ${formatD(limit.manualSpeedLimit.get)}")
    }
    
    Logger.logLine(s"Braking data")
    BrakeAnalyzer.brakeDecelerations.foreach{bd =>
      Logger.logLine(s" ${formatD(bd.speed)}, ${formatD(bd.acceleration)} ")
    }
  }

  override def spawn = {
    currentSpeed = 0
    throttle = 1
  }

  override def handleTurbo() {
    super.handleTurbo()
  }

  override def crashed() {
    if (!bumped) {
      SpeedAnalyzer.decreaseMagicVar
      val curve = SpeedLimiter.curves.find(c => c.index <= currentPieceIndex && c.endIndex >= currentPieceIndex)
      if (curve.isDefined) {
        val lane = curve.get.lanes.find(_.index == currentLane)
        if (lane.isDefined && lane.get.manualSpeedLimit.isDefined) {
          lane.get.manualSpeedLimit = Some(lane.get.manualSpeedLimit.get - 0.5)
        }
      }
    }
  }

  private def logStuff() = {
    Logger.logLine(s"gameTick $gameTick, ${(currentDistance / trackLength * 100).toInt} % of lap $lap, throttle: $throttle, state: $state, mode: $mode")
    Logger.logLine(s"currentSpeed: $currentSpeed currentAngle: $currentAngle")
    Logger.logLine(s"")
  }

  def changeMode(newMode: BehaviourMode.Value) = {
    if (mode != newMode) {
      if (newMode == BehaviourMode.OnCurve) {
        //val (distance, lanes) = getNextSpeedLimit
        val safeSpeed = SpeedLimiter.speedLimitOnPosition(currentDistance, currentLane)
        //val safeSpeed = safeCurveSpeed(lanes.get(currentLane).get).get
        Logger.logLine(s"Entering curve withSpeed: $currentSpeed, limit is: ${safeSpeed}, overspeed: ${currentSpeed > safeSpeed}")
        /*curveStatMaxAngle = 0
        curveStatMaxSpeed = 0
        curveStatMaxThrottle = 0
        curveStatMinThrottle = 1*/
        throttle = 1f
      }
      if (mode == BehaviourMode.OnCurve) {
        /*Logger.logLine(s"Exiting curve withSpeed: $currentSpeed, Curve stats: maxSpeed ${curveStatMaxSpeed}, "
          + s"maxAngle ${curveStatMaxAngle}, throttle ${curveStatMinThrottle}..${curveStatMaxThrottle} ")*/
      }
      mode = newMode
    }
  }

  private def handleInterpolation() {
    if (gameTick % interpolateInterval == interpolateInterval) {
      ThrottleAnalyzer.interpolate
    } else if (gameTick % interpolateInterval == (interpolateInterval / 2)) {
      BrakeAnalyzer.interpolate
    }
  }
}