package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.collection.immutable.List
import scala.collection.mutable.Map
import scala.collection.mutable.Set
import scala.collection.mutable.ListBuffer
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator
import scala.collection.mutable.SortedSet

/*object BehaviourMode extends Enumeration {
  val StraightRoad, NearCurve, OnCurve = Value
}*/

class NoobBot(val host: String, val port: Int, val botName: String, val botKey: String) extends Bot {

  private def changeMode(newMode: BehaviourMode.Value) = {
    if (mode != newMode) {
      Logger.logLine("Changing mode to " + newMode)
      if (newMode == BehaviourMode.OnCurve) {
        val (distance, lanes) = getNextSpeedLimit
        val safeSpeed = safeCurveSpeed(lanes.get(currentLane).get).get
        Logger.logLine(s"Entering curve withSpeed: $currentSpeed, limit is: ${safeSpeed}, overspeed: ${currentSpeed > safeSpeed}")
        curveStatMaxAngle = 0
        curveStatMaxSpeed = 0
        curveStatMaxThrottle = 0
        curveStatMinThrottle = 1
        throttle = 1f
      }
      if (mode == BehaviourMode.OnCurve) {
        Logger.logLine(s"Exiting curve withSpeed: $currentSpeed, Curve stats: maxSpeed ${curveStatMaxSpeed}, "
          + s"maxAngle ${curveStatMaxAngle}, throttle ${curveStatMinThrottle}..${curveStatMaxThrottle} ")
      }
      mode = newMode
    }
  }

  var mode = BehaviourMode.StraightRoad

  
  var previousAngle: Double = 0

  var currentRealAngle: Double = 0
  var previousRealAngle: Double = 0


  var ticksToAccelerate = 10
  var brakingTicks = 0
  var ticksToBreak = 5
  var brakeDelay = 0d
  var state = 0

  var curveStatMaxAngle = 0d
  var curveStatMaxSpeed = 0d
  var curveStatMaxThrottle = 0d
  var curveStatMinThrottle = 0d
  
  var switchToLeftOnPieces = new ListBuffer[Int]()
  var switchToRightOnPieces = new ListBuffer[Int]()

  val accelerations = scala.collection.mutable.Map[Int, Double]()
  val decelerations = scala.collection.mutable.Map[Int, Double](0 -> -0.05)

  val accChanges = SortedSet[SpeedAccPair]()
  var interpolatedAccChange: PolynomialSplineFunction = null
  val decChanges = SortedSet[SpeedAccPair]()
  var interpolatedDecChange: PolynomialSplineFunction = null

  var laneChanges = 0

  val curveSpeeds = scala.collection.mutable.Map[Int, Double](0 -> 3d, 40 -> 4d, 60 -> 5.4d, 90 -> 5.7d, 110 -> 6.8d, 180 -> 8.6d, 200 -> 9.1d, 220 -> 9.6d)

  // Oppimistesti, matalat alkunopeudet
  //val curveSpeeds = scala.collection.mutable.Map[Int, Double](0 -> 3d, 90 -> 6d, 110 -> 8d, 190 -> 16d)

  // Changed to Map[Position, Map[lane, radius]}
  val speedLimits = scala.collection.mutable.Map[Double, scala.collection.immutable.Map[Int, Int]]()
  
  override def initialization(init: GameInit) = {
    super.initialization(init)

    course.foreach { c =>
      if (!c._2.straight) {
        val radius = c._2.piece.radius.get
        //Changed to Map[Position, Map[lane, radius]}
        val laneRadiuses = lanes.map(l => (l.index, c._2.radius(l.distanceFromCenter).get)).toMap
        speedLimits.put(c._2.absolutePosition, laneRadiuses)
      }
    }
    Logger.logLine("SpeedLimitedAreas")
    val speedLimitKeysOrdered = speedLimits.map(_._1).toList.sortWith(_ < _)
    speedLimitKeysOrdered.foreach { key =>
      Logger.logLine("  LimitBegins at distance: " + key)
      speedLimits.get(key).get.foreach { t =>
        Logger.logLine("   lane: " + t._1 + ", radius: " + t._2)
      }
    }
    course.filter(c => c._2.piece.switch.isDefined && c._2.piece.switch.get).foreach{ c =>
    	var index = (c._1 + 1) % course.size
    	// This doesn't calculate track lengths
    	// Instead this calculates distance of track turning into direction
    	// So if leftDistance is greater than right, ai should take left lane
    	var leftDistance = 0d
    	var rightDistance = 0d
    	var cpiece = course.get(index).get
    	while ( !cpiece.piece.switch.isDefined || !cpiece.piece.switch.get ) {
    	  if (cpiece.piece.angle.isDefined && cpiece.piece.angle.get > 0) {
    	    rightDistance += cpiece.length
    	  } else if (cpiece.piece.angle.isDefined && cpiece.piece.angle.get < 0) {
    	    leftDistance += cpiece.length
    	  }
    	  index = (index + 1) % course.size
    	  cpiece = course.get(index).get
    	}
    	
    	if (leftDistance > rightDistance) {
    	  switchToLeftOnPieces.append(c._1)
    	  Logger.logLine(s"On switch ${c._1} should change to left (${formatD(leftDistance)} - ${formatD(rightDistance)})")
    	} else if (rightDistance > leftDistance) {
    	  switchToRightOnPieces.append(c._1)
    	  Logger.logLine(s"On switch ${c._1} should change to right (${formatD(leftDistance)} - ${formatD(rightDistance)})")
    	} else {
    	  Logger.logLine(s"On switch ${c._1} no need to change (${formatD(leftDistance)} - ${formatD(rightDistance)})")
    	}
    	
    }
    
     

  }

  private def updateCurveStats() = {
    if (math.abs(currentAngle) > curveStatMaxAngle) {
      curveStatMaxAngle = math.abs(currentAngle)
    }

    if (currentSpeed > curveStatMaxSpeed) {
      curveStatMaxSpeed = currentSpeed
    }

    if (throttle >= curveStatMaxThrottle) {
      curveStatMaxThrottle = throttle
    }

    if (throttle <= curveStatMinThrottle) {
      curveStatMinThrottle = throttle
    }

  }
  private def shouldUseTurbo(): Boolean = {
    val fastestPieces = straights.filter(_._2 == straights.map(_._2).max).map(_._1).toList
    if (fastestPieces.contains(currentPieceIndex) || fastestPieces.contains(currentPieceIndex + 1)) {
      true
    } else {
      var index = (currentPieceIndex + 1) % course.size
      var straightDistance = 0d
      while (course.get(index).get.straight) {
        straightDistance += course.get(index).get.length
        index = (index + 1) % course.size
      }

      straightDistance > 400
    }
  }

  override def handlePositions(cps: CarPositions) = {
    super.handlePositions(cps)
    val cp = cps.data.filter(_.id.color == myCar.color).head

    val currentPiece = course(cp.piecePosition.pieceIndex)

    // No track changes
    val nextIndex = (cp.piecePosition.pieceIndex+1) % course.size
    val nextPiece = course(nextIndex)
    if (nextPiece.piece.switch.isDefined && nextPiece.piece.switch.get && previousPieceIndex != currentPieceIndex) {
      Logger.logLine("Switch on index " + nextIndex)
      if (switchToLeftOnPieces.contains(nextIndex)) {
        send(MsgWrapper("switchLane", "Left"))
      } else if (switchToRightOnPieces.contains(nextIndex)) {
        send(MsgWrapper("switchLane", "Right"))
      }
      
    }

    previousAngle = currentAngle
    previousRealAngle = currentRealAngle
    currentRealAngle = cp.angle

    currentAngle = math.abs(cp.angle)
    currentLane = cp.piecePosition.lane.endLaneIndex

    if (currentDistance < previousDistance && cp.piecePosition.pieceIndex == 0) {
      currentSpeed = currentDistance + trackLength - previousDistance
    }

    // HACK PATCH
    // Speedcalculation has some errors
    val speedError = math.abs(currentSpeed - previousSpeed) > 2
    if (speedError) {
      Logger.logLine(s"SpeedError, CurrentDistance: ${formatD(currentDistance)}, PreviousDistance: ${formatD(previousDistance)}, " +
        s"currentSpeed: ${formatD(currentSpeed)}, previousSpeed: ${formatD(previousSpeed)}")
      Logger.logLine(s"PieceIndex: ${cp.piecePosition.pieceIndex}, pieceAbsolutePos: ${formatD(currentPiece.absolutePosition)}, inPieceDistance: ${formatD(cp.piecePosition.inPieceDistance)}")
    }
    
    if (!isCrashed && throttle == 1 && !speedError) {
      accelerations.put(currentSpeed.toInt, currentSpeed - previousSpeed)
      accChanges += SpeedAccPair(previousSpeed, currentSpeed - previousSpeed)
    }
    if (!isCrashed && throttle == 0 && currentSpeed - previousSpeed < 0 && currentAngle < 10 && !speedError) {
      val existing = decelerations.get(previousSpeed.toInt)
      if (existing.isEmpty || existing.get < currentSpeed - previousSpeed) {
        decelerations.put(previousSpeed.toInt, currentSpeed - previousSpeed)
      }
      decChanges += SpeedAccPair(previousSpeed, currentSpeed - previousSpeed)
    }

    // Use first few ticks to accelerate
    // Then set throttle to 0 and calculate how many turns it takes 
    // to speed starting to slow down
    // then brake few ticks
    state match {
      case 0 => {
        throttle = 1
        ticksToAccelerate -= 1
        if (ticksToAccelerate == 0) {
          state = 1
          throttle = 0
        }

        accelerations.put(currentSpeed.toInt, currentSpeed - previousSpeed)

        logStuff
      }
      case 1 => {
        throttle = 0

        if (previousSpeed >= currentSpeed) {
          state = 2
        } else {
          brakeDelay += 1
        }
        logStuff
      }
      case 2 => {
        brakingTicks += 1
        throttle = 0

        if (brakingTicks >= ticksToBreak) {
          state = 3
          throttle = 1
          Logger.logLine("init done")
          Logger.logLine(s"brakeDelay = $brakeDelay")
        }
        decelerations.put(previousSpeed.toInt, currentSpeed - previousSpeed)

        logStuff
      }
      case 3 => {

        if (needToBrake) {
          changeMode(BehaviourMode.NearCurve)
          throttle = 0
        } else if (!currentPiece.straight) {

          updateCurveStats()

          changeMode(BehaviourMode.OnCurve)
          val laneOffset = lanes.find(_.index == currentLane).get.distanceFromCenter
          val radius = currentPiece.radius(laneOffset).get

          // handle throttle by drifting angle
          //val changeOfAngle = currentAngle - previousAngle
          val changeOfAngle = math.abs(currentRealAngle - previousRealAngle)

          // Leaving curve. Accelerate at the end of curve
          if (changeOfAngle < 2 && isCurveEnding && currentAngle < 40) {
            if (turbo.isDefined && shouldUseTurbo) {
              handleTurbo()
            }
            throttle = 1

          } else if (changeOfAngle > 3) {
            throttle = throttle / 2
          } else if (changeOfAngle > 2 || currentAngle > 40) {
            throttle -= 0.1f
            // Change of angle greater than 3 usually means crash
          } else if (changeOfAngle > 3 || currentAngle > 50) {
            throttle = 0f
          } else {
            val speedChange = currentSpeed - previousSpeed

            val safeSpeed = safeCurveSpeed(radius).get
            // If driving too fast and speed isn't dropping down -> decelerate
            if (currentSpeed > safeSpeed && speedChange >= 0) {
              throttle -= 0.1f
            } else if (currentSpeed < safeCurveSpeed(radius).get && speedChange <= 0) {
              throttle += 0.1f
            }
          }

          throttle = math.min(1, throttle)
          throttle = math.max(0, throttle)

          if (changeOfAngle < 2 || currentAngle < 30) {
            if (!speedError) updateCurveSpeed(radius)
          }

          if (isCrashed) {
            // TODO hidasta kurvinopeutta
            curveSpeeds.put(radius, (previousSpeed * 0.9))
            throttle = 1
          }

          if (!isCrashed) {
            val laneOffset = lanes.find(_.index == currentLane).get.distanceFromCenter
            Logger.logLine("On curve. Radius " + currentPiece.piece.radius.get +
              ", laneRadius: " + currentPiece.radius(laneOffset).get +
              ", Drift angle " + formatD(cp.angle) +
              ", Change of angle " + formatD(changeOfAngle) +

              " Speed " + formatD(currentSpeed) +
              " throttle " + formatD(throttle))
          }

        } else {

          if (turbo.isDefined && shouldUseTurbo) {
            handleTurbo()
          }
          changeMode(BehaviourMode.StraightRoad)
          throttle = 1
        }
      }
    }

    /*
    // Moved to right place
    if (previousSpeed > 0 && gameTick < 1000) {
      if ((currentSpeed - previousSpeed) > 0) {
        accChanges += SpeedAccPair(previousSpeed, currentSpeed - previousSpeed)
      } else if ((currentSpeed - previousSpeed) < 0) {
        decChanges += SpeedAccPair(previousSpeed, currentSpeed - previousSpeed)
      }
    }
    */
    
    
    if (accChanges.size > 10 && accChanges.size % 10 == 0) {
      //Logger.logLine(s"Acc $accChanges")
      interpolatedAccChange = (new SplineInterpolator()).interpolate(accChanges.map(_.speed).toArray, accChanges.map(_.acceleration).toArray)
    }

    if (decChanges.size > 10 && decChanges.size % 10 == 0) {
      //Logger.logLine(s"Decc $decChanges")
      interpolatedDecChange = (new SplineInterpolator()).interpolate(decChanges.map(_.speed).toArray, decChanges.map(_.acceleration).toArray)
    }

    throttle = math.min(1, throttle)
    throttle = math.max(0, throttle)

    if (gameTick % 10 == 0) {
      logStuff
    }
  }

  override def finish() = {
    Logger.logLine("Optimized speeds")
    curveSpeeds.foreach(cs => Logger.logLine(s"  radius ${cs._1} -> speed ${cs._2} "))
    Logger.logLine("Accelerations")
    accelerations.toList.sortBy(_._1).foreach(a => Logger.logLine(s"  ${a._1} ${a._2}"))
    Logger.logLine("Decelerations")
    decelerations.toList.sortBy(_._1).foreach(a => Logger.logLine(s"  ${a._1} ${a._2}"))
    //
    //    Logger.logLine("Acceleration interpolation")
    //    interpolatedAccChange.getPolynomials().foreach(f => Logger.logLine(f.toString()))
    //
    //    Logger.logLine("Deceleration interpolation")
    //    interpolatedDecChange.getPolynomials().foreach(f => Logger.logLine(f.toString()))

    //    Logger.logLine(interpolatedAccChange.value(25))

    //    Logger.logLine(interpolatedDecChange.value(25))

    Logger.logLine(s"Crashed ${crashes} times")
  }

  override def spawn = {
    currentSpeed = 0
    currentAngle = 0
  }

  override def handleTurbo() {
    super.handleTurbo()

    if (needToBrake || !course.get(currentPieceIndex).get.straight) {
      throttle = (throttle / turboFactor).toFloat
    }
  }

  // (Distance, Map[Lane, radius])
  private def getNextSpeedLimit(): (Double, scala.collection.immutable.Map[Int, Int]) = {
    val nextLimit = speedLimits.toList.filter(sl => sl._1 >= currentDistance).sortBy(_._1).headOption
    if (nextLimit.isDefined) {
      (nextLimit.get._1 - currentDistance, nextLimit.get._2)
    } else {
      val firstLimitDistance = speedLimits.map(_._1).min
      (firstLimitDistance + trackLength - currentDistance, speedLimits.get(firstLimitDistance).get)
    }
  }

  def calculateDistanceToNextSpeedLimit(): (Double, Option[Double]) = {
    val (distance, lanes) = getNextSpeedLimit
    val laneRadius = lanes.get(currentLane).get
    (distance, safeCurveSpeed(laneRadius))
  }

  private def isCurveEnding: Boolean = {
    if (needToBrake) {
      false
    } else {
      val nextPieceCurve = course.get((currentPieceIndex + 1) % course.size).get.straight
      if (!nextPieceCurve && !course.get(currentPieceIndex).get.straight) {
        val piece = course.get(currentPieceIndex).get
        val laneOffset = lanes.find(_.index == currentLane).get.distanceFromCenter
        val r = piece.radius(laneOffset).get
        val a = math.abs(piece.piece.angle.get)
        val curveLeft = math.Pi * 2 * r * a / 360 - currentCP.get.piecePosition.inPieceDistance
        val ticksOnCurve = curveLeft / currentSpeed
        // 12 && 8
        if (ticksOnCurve < 4) {
          true
        } else if (ticksOnCurve < 30 && currentAngle < 30) {
          true
        } else if (ticksOnCurve < 10 && currentAngle < 45) {
          true
        } else {
          false
        }
      } else {
        false
      }
    }

  }

  // calculates ticks to next curve
  // calculates brakeDelay and deceleration for current speed
  // checks if we can still use max throttle
  private def needToBrake(): Boolean = {
    val nextLimit = calculateDistanceToNextSpeedLimit
    if (nextLimit._2.isEmpty || nextLimit._2.get > currentSpeed) {
      false
    } else {

      val distanceToNextLimit = nextLimit._1 //- currentDistance
      //val ticksLeft = (distanceToNextLimit / currentSpeed - brakeDelay).toInt
      val ticksLeft = (distanceToNextLimit / currentSpeed).toInt

      val nearestDecelerationForCurrentSpeed = decelerations.filter(_._1 <= currentSpeed).map(_._1).max
      val nearestDecelerationForTargetSpeed = decelerations.filter(_._1 <= nextLimit._2.get).map(_._1).max

      // Try to calculate average braking deceleration
      val brakeDeceleration = (decelerations.get(nearestDecelerationForCurrentSpeed).get + decelerations.get(nearestDecelerationForTargetSpeed).get) / 2
      val speedOnLimitIfBreaking = currentSpeed + ticksLeft * brakeDeceleration

      val needToBrake = speedOnLimitIfBreaking > nextLimit._2.get
      if (needToBrake) {
        Logger.logLine(s"NeedToBrake. Speed: ${formatD(currentSpeed)}, limit: ${nextLimit._2.get}, ticksBeforeCurve: ${ticksLeft}, " +
          s"distanceToNextLimit: ${formatD(nextLimit._1)}, brakeDeceleration: ${formatD(brakeDeceleration)} speedOnCurve: ${formatD(speedOnLimitIfBreaking)}")
      }
      needToBrake

    }
  }

  private def estimateSpeedIfBraking(ticks: Int, speed: Double = currentSpeed): Double = {
    var remainingSpeed = math.max(speed, 0)
    for (i <- 1 to ticks) {
      val nearestDecelerationForRemainingSpeed = decelerations.filter(_._1 <= remainingSpeed).map(_._1).max
      val deceleration = decelerations.get(nearestDecelerationForRemainingSpeed).get
      remainingSpeed = math.max(remainingSpeed + deceleration, 0)

    }

    remainingSpeed
  }


  // Gets safe speed for curve with radius
  // Uses Map that should learn optimum speeds while driving
  private def safeCurveSpeed(radius: Int): Option[Double] = {
    val exact = curveSpeeds.get(radius)
    if (exact.isDefined) {
      exact
    } else {
      val nearestLower = curveSpeeds.filter(cs => cs._1 < radius).map(_._1).max
      curveSpeeds.get(nearestLower)
    }
  }

  private def updateCurveSpeed(radius: Int) = {
    val storedOptimumSpeed = curveSpeeds.get(radius)
    /*if (storedOptimumSpeed.isDefined && storedOptimumSpeed.get < currentSpeed) {
      Logger.logLine(s"Updating safe speed for $radius radius to $currentSpeed")
      //curveSpeeds.put(radius, currentSpeed)
      curveSpeeds.put(radius, (currentSpeed + storedOptimumSpeed.get) / 2)
    }*/
  }

  private def logStuff() = {
    Logger.logLine(s"gameTick $gameTick, ${(currentDistance / trackLength * 100).toInt} % of lap $lap, throttle: $throttle, state: $state, mode: $mode")
    Logger.logLine(s"currentSpeed: $currentSpeed currentAngle: $currentAngle")
    Logger.logLine(s"")
  }

  def calculateDistance(position: PiecePosition): Double = {
    currentDistance - previousDistance
  }

}